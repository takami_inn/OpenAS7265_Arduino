#include <Wire.h>
#include <EEPROM.h>
#include "ArduinoUniqueID.h"
#include "SparkFun_AS7265X.h"
AS7265X sensor;

int i = 0;
byte buffer[32];

void setup(){
  Serial.begin(115200);
  delay(100);  // give the capicity some time to charge up
  if(sensor.begin() == false){
    Serial.println("Sensor Error");
    while(1){}
  }
  sensor.setIntegrationCycles(200);
  // you can setup sensor settings. see example for Sparkfun_AS7265x if you use downloaded lib
  // you can also read the sourcecode in this folder if you can't be bothered to install a new lib

}
void loop(){
  while(Serial.available()>0){
    delay(2);    // wait 2ms for serial communicating
    for(i = 0;i<=15;i++){
      buffer[i] = Serial.read();
    }
    // get raw values.
    if(buffer[0] == 'R' && buffer[1] == 'a' && buffer[2] == 'w'){
      while(Serial.read() >=0){}
      sensor.takeMeasurements();
      Serial.print(sensor.getA());
      Serial.print(",");
      Serial.print(sensor.getB());
	  Serial.print(",");
	  Serial.print(sensor.getC());
	  Serial.print(",");
	  Serial.print(sensor.getD());
	  Serial.print(",");
	  Serial.print(sensor.getE());
	  Serial.print(",");
	  Serial.print(sensor.getF());
	  Serial.print(",");
	  Serial.print(sensor.getG());
	  Serial.print(",");
	  Serial.print(sensor.getH());
	  Serial.print(",");
	  Serial.print(sensor.getI());
	  Serial.print(",");
	  Serial.print(sensor.getJ());
	  Serial.print(",");
	  Serial.print(sensor.getK());
	  Serial.print(",");
	  Serial.print(sensor.getL());
	  Serial.print(",");
	  Serial.print(sensor.getR());
	  Serial.print(",");
	  Serial.print(sensor.getS());
	  Serial.print(",");
	  Serial.print(sensor.getT());
	  Serial.print(",");
	  Serial.print(sensor.getU());
	  Serial.print(",");
	  Serial.print(sensor.getV());
	  Serial.print(",");
	  Serial.print(sensor.getW());
	  Serial.print(",");
	  Serial.println();
    }
    //get calbrated value
    else if(buffer[0] == 'C' && buffer[1] == 'a' && buffer[2] == 'l' && 
              buffer[3] == 'b' && buffer[4] == 'e' && buffer[5] == 'd'){
      while(Serial.read() >=0){}  // flush the serial buffer
	  sensor.takeMeasurements();
	  Serial.print(sensor.getCalibratedA());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedB());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedC());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedD());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedE());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedF());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedG());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedH());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedI());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedJ());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedK());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedL());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedR());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedS());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedT());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedU());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedV());
	  Serial.print(",");
	  Serial.print(sensor.getCalibratedW());
	  Serial.print(",");
	  Serial.println();

    }
	// if command is not known, warn the error.
    else{
      Serial.print("ERROR!\n");
      delay(10);
    }
  }
}
