import numpy as np
import matplotlib.pyplot as plot
# import scipy
import pandas as pd

xn = 96.4227
yn = 100.000
zn = 82.5221

def f(t):
    if(t > ((6/29)**3)):
        return t**(1/3)
    else:
        return ((1/3)*((29/6)**2))*t + (16/116)



data2 = pd.read_csv('SpecXYZ-12Channel.csv')
wl2 = data2.WaveLength
x2 = data2.x
y2 = data2.y
z2 = data2.z
# plot.scatter(wl2, x2)
# plot.scatter(wl2, y2)
# plot.scatter(wl2, z2)

dataT = pd.read_csv('PurpleTest.csv') # input data
A = dataT.A[0]
B = dataT.B[0]
C = dataT.C[0]
D = dataT.D[0]
E = dataT.E[0]
F = dataT.F[0]
G = dataT.G[0]
H = dataT.H[0]
R = dataT.R[0]
I = dataT.I[0]
S = dataT.S[0]
J = dataT.J[0]
#print (dataT)
xx = 0
xx = xx + x2[0]*A
xx = xx + x2[1]*B
xx = xx + x2[2]*C
xx = xx + x2[3]*D
xx = xx + x2[4]*E
xx = xx + x2[5]*F
xx = xx + x2[6]*G
xx = xx + x2[7]*H
xx = xx + x2[8]*R
xx = xx + x2[9]*I
xx = xx + x2[10]*S
xx = xx + x2[11]*J
yy = 0
yy = yy + y2[0]*A
yy = yy + y2[1]*B
yy = yy + y2[2]*C
yy = yy + y2[3]*D
yy = yy + y2[4]*E
yy = yy + y2[5]*F
yy = yy + y2[6]*G
yy = yy + y2[7]*H
yy = yy + y2[8]*R
yy = yy + y2[9]*I
yy = yy + y2[10]*S
yy = yy + y2[11]*J
zz = 0
zz = zz + z2[0]*A
zz = zz + z2[1]*B
zz = zz + z2[2]*C
zz = zz + z2[3]*D
zz = zz + z2[4]*E
zz = zz + z2[5]*F
zz = zz + z2[6]*G
zz = zz + z2[7]*H
zz = zz + z2[8]*R
zz = zz + z2[9]*I
zz = zz + z2[10]*S
zz = zz + z2[11]*J

print('LOW concentration')
print('X,Y and Z :')
print(xx)
print(yy)
print(zz)
L = 116* f((yy/yn)) - 16
a = 500* (f((xx/xn)) - f((yy/yn)))
b = 200* (f((yy/yn)) - f((zz/zn)))
xxx = xx/(xx+yy+zz)
yyy = yy/(xx+yy+zz)
print('Lab:')
print(L)
print(a)
print(b)
print('x and y:')
print(xxx)
print(yyy)

A = dataT.A[1]
B = dataT.B[1]
C = dataT.C[1]
D = dataT.D[1]
E = dataT.E[1]
F = dataT.F[1]
G = dataT.G[1]
H = dataT.H[1]
R = dataT.R[1]
I = dataT.I[1]
S = dataT.S[1]
J = dataT.J[1]
#print (dataT)
xx = 0
xx = xx + x2[0]*A
xx = xx + x2[1]*B
xx = xx + x2[2]*C
xx = xx + x2[3]*D
xx = xx + x2[4]*E
xx = xx + x2[5]*F
xx = xx + x2[6]*G
xx = xx + x2[7]*H
xx = xx + x2[8]*R
xx = xx + x2[9]*I
xx = xx + x2[10]*S
xx = xx + x2[11]*J
yy = 0
yy = yy + y2[0]*A
yy = yy + y2[1]*B
yy = yy + y2[2]*C
yy = yy + y2[3]*D
yy = yy + y2[4]*E
yy = yy + y2[5]*F
yy = yy + y2[6]*G
yy = yy + y2[7]*H
yy = yy + y2[8]*R
yy = yy + y2[9]*I
yy = yy + y2[10]*S
yy = yy + y2[11]*J
zz = 0
zz = zz + z2[0]*A
zz = zz + z2[1]*B
zz = zz + z2[2]*C
zz = zz + z2[3]*D
zz = zz + z2[4]*E
zz = zz + z2[5]*F
zz = zz + z2[6]*G
zz = zz + z2[7]*H
zz = zz + z2[8]*R
zz = zz + z2[9]*I
zz = zz + z2[10]*S
zz = zz + z2[11]*J
print('\r\n')
print('HIGH concentration')
print('X,Y and Z :')
print(xx)
print(yy)
print(zz)
L = 116* f((yy/yn)) - 16
a = 500* (f((xx/xn)) - f((yy/yn)))
b = 200* (f((yy/yn)) - f((zz/zn)))
xxx = xx/(xx+yy+zz)
yyy = yy/(xx+yy+zz)
print('Lab:')
print(L)
print(a)
print(b)
print('x and y:')
print(xxx)
print(yyy)