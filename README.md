# OpenAS7265_Arduino

#### 介绍
Arduino 驱动 AS7265x ，开源硬件和对应代码

#### 简介
基于Arduino和AMS AS7265x系列传感器实现的开源光谱仪

#### 目录

##### HardWare：
硬件设计图和原理图，使用Altium Designer可以打开

##### SoftWare：
驱动代码等

#### 使用教程：
1 根据自身需求修改PCB，导出gerber后打板

2 在SPI芯片中烧录sensor firmware

3 PCB焊接和组装完成后通过对应引脚为板载单片机下载Arduino固件，推荐下载为Arduino NANO

4 在Arduino IDE中打开software中的内容，编译并下载

5 (可选)：前往 https://gitee.com/takami_inn/spectro-upside ，使用上述python上位机可以得到初步数据。