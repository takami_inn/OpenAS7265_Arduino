# OpenAS7265_Arduino

#### Description
Arduino driven AMS AS7265x chipsets, contains hardware and software.

#### overview
Open source spectro sensor,based on Arduino & AMS AS7265x.

#### Contents

##### HardWare:
Hardware design schmatics and PCB layouts, can be opened with Altium Designer.

##### SoftWare:
All the codes.

#### How to use:
1 Change PCB by your own need.

2 Burn Sensor firmware into the SPI flash chip

3 Make your own PCBA,and download the bootloader for it. Download as Arduino NANO is recommended.

4 Open the code with Arduino IDE, build,download,and go.